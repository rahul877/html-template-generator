Operations to support
1. Crud Operation for htmlTemplate
2. query htmlTemplate (pass template id)
        with or without handlebar 
        with updated timestamp
        with created timestamp
3. pass objects and generate rendered html


// Query to generate html with handlebar "templateName": "address-template",
query{
  generateHtmlWithHandlebar(id:"ck04c1ywxuhlo0b53hdtq2016", data:"{\"city\": \"London\", \"street\": \"Baker Street\", \"number\": \"221B\"}")
}

// Retrieves all templates
query{
  htmlTemplates{
    id,
    templateName,
    templateData,  
    createdAt,
    updatedAt,
  }
}

// Query single html template
query{
  htmlTemplate(id:"ck04c1ywxuhlo0b53hdtq2016"){
    id,
    templateName
  }
}

// Add expression template
mutation{
  addHtmlTemplate(       
    templateName:"expression-template",
    templateData:"{{description.escaped}}{{example}}<br><br>{{description.unescaped}}{{{example}}}",
){
  id
}
}

// Update expression template
mutation{
  updateHtmlTemplate(   
    id:"ck04cb4nluio00b53q731x7u9",
    templateName:"expression-template",
    templateData:"{{description.escaped}}{{example}}<br><br>{{description.unescaped}}{{{example}}}"
){
  id
}
}

// Delete html template
mutation{
  deleteHtmlTemplate(
    id:"ck048gfr7tmtd0b5337n2byvi"    
){
  id
  templateName
}
}

// How put Authorization token

1. Login to get token
mutation {
  login(
    username: "rahul"    
    password: "graphql"
  ) {
    token
    user {
      id
    }
  }
}

2. Copy token and put it under "Http Headers" like below:
   {"Authorization": "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VySWQiOiJjazBjeHVpejIxeWNvMGI1M3FqMHZ0dDQ1IiwiaWF0IjoxNTY4MDY2MzcyfQ.LB1T3AmUj_j7PWLSptE-Bik63lIyFVsvLk0T28zwvqQ"}
   Bearer is just a prefix used in code .. This can be removed. If droping "Bearer " then drop it from utils.js also.

3. Send request.. Thats it !!