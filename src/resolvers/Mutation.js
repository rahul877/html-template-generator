const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const { APP_SECRET, getUserId } = require('../utils').default

async function addHtmlTemplate(parent, args, context, info) {
    const userId = getUserId(context)
    htmlTemplate = {
        templateName: args.templateName,
        templateData: args.templateData,
    }

    htmlTemplate = context.prisma.createHtmlTemplate(htmlTemplate)
    return htmlTemplate
}

async function updateHtmlTemplate(parent, args, context, info) {
    const userId = getUserId(context)
    htmlTemplate = context.prisma.updateHtmlTemplate({ where: { id: args.id }, data: { templateName: args.templateName, templateData: args.templateData } })
    return htmlTemplate
}

async function deleteHtmlTemplate(parent, args, context, info) {
    const userId = getUserId(context)
    deletedTemplate = context.prisma.deleteHtmlTemplate({ id: args.id })
    return deletedTemplate
}

async function signup(parent, args, context, info) {
    // 1
    const password = await bcrypt.hash(args.password, 10)
    // 2
    const user = await context.prisma.createUser({ ...args, password })
  
    // 3
    const token = jwt.sign({ userId: user.id }, APP_SECRET)
  
    // 4
    return {
      token,
      user,
    }
  }
  
  async function login(parent, args, context, info) {
    // 1
    const user = await context.prisma.user({ username: args.username })
    if (!user) {
      throw new Error('No such user found')
    }
  
    // 2
    const valid = await bcrypt.compare(args.password, user.password)
    if (!valid) {
      throw new Error('Invalid password')
    }
  
    const token = jwt.sign({ userId: user.id }, APP_SECRET)
  
    // 3
    return {
      token,
      user,
    }
  }

  
module.exports = {
    addHtmlTemplate,
    updateHtmlTemplate,
    deleteHtmlTemplate,
    signup,
    login,
}