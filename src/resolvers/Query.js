const HandleBars = require('handlebars');
const { APP_SECRET, getUserId } = require('../utils')

async function htmlTemplates(parent, args, context, info) {    
    const userId = getUserId(context)
    return context.prisma.htmlTemplates(parent, args, context, info)
}

async function htmlTemplate(parent, args, context, info) {
    const userId = getUserId(context)    
    return context.prisma.htmlTemplate({ id: args.id })
}

async function generateHtmlWithHandlebar(parent, args, context, info) {
    //args.id = templateId
    //args.data = {}
    const userId = getUserId(context)
    htmlTemplateObject = await context.prisma.htmlTemplate({ id: args.id })    
    compiledHtmlTemplate = HandleBars.compile(htmlTemplateObject.templateData)    
    
    // Pass our data to the template
    var theCompiledHtml = compiledHtmlTemplate(args.data);
    return theCompiledHtml
    
}

module.exports = {
    htmlTemplates,
    htmlTemplate,
    generateHtmlWithHandlebar, 
  }