# html-template-generator
A graphql project with prisma to service html handlebars

To run this project follow these steps:

Setting up environment:
======================
1. Install nodejs latest version
2. Install yarn
3. Execute command "yarn add graphql-yoga" inside directory html-template-generator
4. Execute command "yarn global add prisma" inside directory html-template-generator

Running applciation:
=====================
5. node ./src/index.js

6. prisma intit (only first time)
7. prisma login (only first time)
8. prisma deploy (whenever schema is added/modified/deleted), hook generate with deploy


// HTTP:  https://eu1.prisma.sh/rahul-kulshreshtha/html-templates-graphql/dev
// WS:    wss://eu1.prisma.sh/rahul-kulshreshtha/html-templates-graphql/dev
// https://eu1.prisma.sh/rahul-kulshreshtha/html-templates-graphql/dev/_admin